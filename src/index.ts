import { delay } from './common/service';
import { processSignals } from './process';
import { config as dotenvConfig } from 'dotenv';
import { createPool, prepareData } from './common/db';
import { Pool } from 'pg';

async function makeCalls() {
    dotenvConfig();
    const pool: Pool = await createPool();
    while(true) {
        await processSignals(pool);
        await delay(5000);
    }
}

makeCalls();