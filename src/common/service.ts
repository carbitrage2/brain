function delay(ms: number): Promise<TimerHandler> {
    return new Promise<TimerHandler>( resolve => setTimeout(resolve, ms) );
}

async function throwError(message: string): Promise<void> {
    return new Promise(() => {
        throw new Error(message);
    });
}

export { delay, throwError };