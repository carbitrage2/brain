import TelegramBot from 'node-telegram-bot-api';
import 'dotenv';
import { Pool } from 'pg';
import { setSignalToSent } from './db';

async function sendSignal(pool: Pool, signal: Signal, calculatedDifference: number): Promise<void> {
    return new Promise<void>(resolve => {
        const token: string = process.env.TG_TOKEN!;
        const bot: TelegramBot = new TelegramBot(token, {polling: false});
        const message: string = `Alert!\n'${calculatedDifference.toString()}' difference was set for pair ${signal.pair} and markets: ${signal.markets.join(', ')}.`
        bot.sendMessage(signal.uid, message);
        setSignalToSent(pool, signal.id);
        resolve();
    });
}

export { sendSignal };