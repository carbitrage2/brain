import { Pool, PoolClient, QueryResult } from 'pg';

async function createPool(): Promise<Pool> {
    return new Promise<Pool>(resolve => {
        const pool: Pool = new Pool(
            {
                user: process.env.DB_USER, 
                host: process.env.DB_HOST, 
                port: +process.env.DB_PORT!, 
                database: process.env.DB_NAME, 
                password: process.env.DB_PASSWORD
            }
        );
        resolve(pool);
    });
}

async function prepareData(pool: Pool): Promise<MarketData> {
    return new Promise<MarketData>(async resolve => {
        const query: string = 'SELECT pair, market, price FROM pairs_and_markets';
        pool.connect(async (poolError: Error, client: PoolClient, release: any) => {
            await client.query(query)
                .then(async (result: QueryResult) => {
                    const receivedData: MarketDataRow[] = result.rows as MarketDataRow[];
                    let processedData: MarketData = {};
                    for await (const entry of receivedData) {
                        if (processedData.hasOwnProperty(entry.pair)) {
                            processedData[entry.pair].push({ market: entry.market, price: entry.price });
                        } else {
                            processedData[entry.pair] = [{ market: entry.market, price: entry.price }];
                        }
                    }
                    resolve(processedData);
                })
                .catch((clientError: Error) => {
                    console.error(`Database error: ${clientError.stack}`);
                });
            release();
        });
    });
}

async function getAllSignals(pool: Pool): Promise<Signal[]> {
    return new Promise<Signal[]>(async resolve => {
        const query: string = 'SELECT id, uid, pair, markets, difference, sent FROM signals';
        pool.connect(async (poolError: Error, client: PoolClient, release: any) => {
            await client.query(query)
                .then((result: QueryResult) => {
                    const data: Signal[] = result.rows as Signal[];
                    resolve(data);
                })
                .catch((clientError: Error) => {
                    console.error(`Database error: ${clientError.stack}`);
                });
            release();
        });
    });
}

async function setSignalToSent(pool: Pool, id: number): Promise<void> {
    return new Promise<void>(resolve => {
        const query: string = `UPDATE signals SET sent = TRUE WHERE id = ${id.toString()}`;
        pool.connect(async (poolError: Error, client: PoolClient, release: any) => {
            await client.query(query)
                .then(() => {
                    resolve();
                })
                .catch((clientError: Error) => {
                    console.error(`Database error: ${clientError.stack}`);
                });
            release();
        });
    });
}

async function setSignalToNonSent(pool: Pool, id: number): Promise<void> {
    return new Promise<void>(resolve => {
        const query: string = `UPDATE signals SET sent = FALSE WHERE id = ${id.toString()}`;
        pool.connect(async (poolError: Error, client: PoolClient, release: any) => {
            await client.query(query)
                .then(() => {
                    resolve();
                })
                .catch((clientError: Error) => {
                    console.error(`Database error: ${clientError.stack}`);
                });
            release();
        });
    });
}

export { createPool, prepareData, getAllSignals, setSignalToSent, setSignalToNonSent };