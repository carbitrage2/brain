declare interface Signal {
    id: number;
    uid: number;
    pair: string;
    markets: string[];
    difference: number;
    sent: boolean;
}

declare interface MarketAndPrice {
    market: string;
    price: number;
}

declare type MarketData = {
    [key: string]: MarketAndPrice[];
}