declare interface MarketDataRow {
    pair: Pair;
    market: string;
    price: number;
}